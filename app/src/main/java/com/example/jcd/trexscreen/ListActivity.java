package com.example.jcd.trexscreen;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;


public class ListActivity extends Activity {

    private static ListView _listView;
    ArrayAdapter<String> _arrayAdapter;
    String[] _filenames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        _listView = (ListView) findViewById(R.id.list_view);
//        String[] names = new String[]{"one", "two"};
        _arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, getFilenames());
        _listView.setAdapter(_arrayAdapter);
        _listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String filename = getVideos()[position].getAbsolutePath();
//                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("file://" + filename)));
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("file://" + filename)));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        registerForContextMenu(_listView);
    }

    List<String> getFilenames() {
        File[] files = getVideos();
        List<String> res = new ArrayList<String>();
        for(int i = 0; i < files.length; i++) {
            res.add(files[i].getName());
        }
        return res;
    }

    File[] getVideos() {
        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
//                boolean res = filename.endsWith(".avi") || filename.endsWith(".mp4") || filename.endsWith(".flv");
                boolean res = !filename.startsWith(".");
                return res;
            }
        };
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).listFiles(filter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, 0, 0, "Play");
        menu.add(0, 1, 0, "Share");
        menu.add(0, 2, 0, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int index = info.position;
        final String filename = getVideos()[index].getAbsolutePath();
        if(0 == item.getItemId()) {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("file://" + filename)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if(1 == item.getItemId()) {
//            Toast.makeText(getApplicationContext(), "Coming soon", Toast.LENGTH_LONG).show();
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            Uri uri = Uri.parse("file://" + filename);
            shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
            shareIntent.setType("video/mp4");
            startActivity(Intent.createChooser(shareIntent, "Send to..."));
        } else if(2 == item.getItemId()) {
//            Toast.makeText(getApplicationContext(), "Delete file " + filename, Toast.LENGTH_LONG).show();
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Delete file?")
                    .setMessage(filename)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            _arrayAdapter.remove(new File(filename).getName());
                            new File(filename).delete();
                            _arrayAdapter.notifyDataSetChanged();
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }
        return super.onContextItemSelected(item);
    }

    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_list, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
