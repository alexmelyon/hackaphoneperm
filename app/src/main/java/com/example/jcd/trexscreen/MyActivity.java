package com.example.jcd.trexscreen;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MyActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        //
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showSettings();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v) {
        if (v.getId() == R.id.openMediaProjection) {
            Intent intent = new Intent(this, MediaProjectionDemo.class);
            startActivity(intent);
        } else if (v.getId() == R.id.openRecorder) {
//            Intent intent = new Intent(this, Recorder.class);
//            startActivity(intent);
        } else if(v.getId() == R.id.open1) {
            Intent intent = new Intent(this, Ac1.class);
            startActivity(intent);
        } else if(v.getId() == R.id.open2) {
            Intent intent = new Intent(this, Ac2.class);
            startActivity(intent);
        }
    }

    public void showSettings()
    {
        Intent i = new Intent(this, Prefs.class);
        startActivity(i);
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        // читаем установленное значение из CheckBoxPreference
        PrefManager.getInstance().Micrphone = prefs.getBoolean("microphone", true);
        PrefManager.getInstance().Shake = prefs.getBoolean("Shake", true);
        PrefManager.getInstance().DropFolder = prefs.getString("dropfolder", "TRecs");
        PrefManager.getInstance().density = prefs.getString("density", "360p");
        PrefManager.getInstance().FormatName = prefs.getString("format", "mp4");
        }

}
