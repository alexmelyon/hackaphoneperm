package com.example.jcd.trexscreen;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by JCD on 13.12.2014.
 */
public class Ac2 extends Activity {
    private final String VIDEO_PATH_DIR = Environment.DIRECTORY_MOVIES;
    private static String VIDEO_PATH_NAME;
    private static final String TAG = "MediaProjectionDemo";
    private static MediaRecorder mMediaRecorder;
    private static MediaProjection mMediaProjection;
    private static Surface mSurface;
    private static MediaProjectionManager mProjectionManager;
    private static VirtualDisplay mVirtualDisplay;
    private static int timerSeconds = 10;
    private static Date startRecordingTime;

    private SeekBar timerBar;
    private ToggleButton mToggleButton;

    private static final int PERMISSION_CODE = 1;
    private int mDisplayWidth = 360;
    private int mDisplayHeight = 640;
    private int mScreenDensity;
    private int mId = 59;

    private static boolean mRecorderInitialised = false;
    private static boolean _isRecording = false;

    private static Timer startTimer;
    private static Timer recordingTimer;

    class StartRecordingTask extends TimerTask {
        @Override
        public void run() {
            startRecording();
        }
    }

    class RefreshRecordingProgressTask extends TimerTask {
        @Override
        public void run() {
            createNotification();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recorder_view);

        final TextView secondsTextView = (TextView) findViewById(R.id.secondsTV);
        SeekBar seekBar = (SeekBar)findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                secondsTextView.setText("Starts after: " + progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mScreenDensity = metrics.densityDpi;

        mDisplayWidth = metrics.widthPixels;
        mDisplayHeight = metrics.heightPixels;

        mToggleButton = (ToggleButton) findViewById(R.id.toggleRecordingButton);
        timerBar = (SeekBar) findViewById(R.id.seekBar);

        timerBar.setProgress(timerSeconds);

        timerBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView timerText = (TextView) findViewById(R.id.secondsTV);
                timerText.setText(String.format("Starts after: %d secs", seekBar.getProgress()));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        if (_isRecording) {
            mToggleButton.setChecked(true);
            timerBar.setVisibility(View.INVISIBLE);
//            secondsTextView.setText();
        }

        mToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((ToggleButton) v).isChecked()) {
                    startTimer = new Timer();
                    StartRecordingTask task = new StartRecordingTask();
                    timerSeconds = timerBar.getProgress();
                    Calendar c = Calendar.getInstance();
                    c.add(Calendar.SECOND, timerSeconds);
                    startTimer.schedule(task, c.getTime());
                } else {
                    stopRecording();
                }
            }
        });
        Button playButton = (Button)findViewById(R.id.playButton);
        playButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
//                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("file://" + VIDEO_PATH_NAME)));
                startActivity(new Intent(Ac2.this, ListActivity.class));
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(_isRecording) {
            stopScreenSharing();
            mMediaRecorder.stop();
            mMediaRecorder.reset();
        }
    }

    private void initRecorder() {
        mRecorderInitialised = false;

        if (mMediaRecorder == null)
            mMediaRecorder = new MediaRecorder();


        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        //
        //
        mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.DEFAULT);

//        mMediaRecorder.setVideoEncodingBitRate(512 * 1000);
        mMediaRecorder.setVideoEncodingBitRate(3000000);
        mMediaRecorder.setVideoFrameRate(30);
        mMediaRecorder.setVideoSize(mDisplayWidth, mDisplayHeight);

        VIDEO_PATH_NAME = getFilePath();
        saveFilePath(VIDEO_PATH_NAME);
        mMediaRecorder.setOutputFile(VIDEO_PATH_NAME);

        try {
            mMediaRecorder.prepare();
            mSurface = mMediaRecorder.getSurface();
            mRecorderInitialised = true;
        } catch (IllegalStateException e) {
            e.printStackTrace();
            mMediaRecorder.reset();

        } catch (IOException e) {
            e.printStackTrace();
            mMediaRecorder.reset();

        }
    }
    void saveFilePath(String filePath) {
        SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = pref.edit();
        edit.putString("trex_file_path", filePath);
    }

    String loadFilePath() {
        SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
        return pref.getString("trex_file_path", null);
    }

    private String getFilePath() {
        Calendar c = Calendar.getInstance();
        Date now = c.getTime();
        String currentDateAndTime = new SimpleDateFormat("yyyyMMdd_HHmmss").format(now);
        return Environment.getExternalStoragePublicDirectory(VIDEO_PATH_DIR) + File.separator + "RECORD_" + currentDateAndTime + ".mp4";
    }

    private void shareScreen() {
        if (mSurface == null) {
            return;
        }

        if (mProjectionManager == null)
            mProjectionManager = (MediaProjectionManager) getSystemService(Context.MEDIA_PROJECTION_SERVICE);

        startActivityForResult(mProjectionManager.createScreenCaptureIntent(), PERMISSION_CODE);

    }

    private void stopScreenSharing() {
        if (mVirtualDisplay == null) {
            return;
        }
        mVirtualDisplay.release();
        mMediaProjection.stop();
        mMediaProjection = null;
        mVirtualDisplay = null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != PERMISSION_CODE) {
            Log.e(TAG, "Unknown request code: " + requestCode);
            return;
        }
        if (resultCode != RESULT_OK) {
            Toast.makeText(this,
                    "User denied screen sharing permission", Toast.LENGTH_SHORT).show();
            return;
        }
        mMediaProjection = mProjectionManager.getMediaProjection(resultCode, data);
        mVirtualDisplay = createVirtualDisplay();
        mMediaRecorder.start();
        _isRecording = true;
        createNotification();
    }

    private void startRecording() {
        if (_isRecording)
            return;

        initRecorder();

        if (!mRecorderInitialised) {
            Toast.makeText(this,
                    "Error while recorder initialization.", Toast.LENGTH_SHORT).show();
            return;
        }

        shareScreen();
        startRecordingTime = Calendar.getInstance().getTime();

        recordingTimer = new Timer();
        RefreshRecordingProgressTask task = new RefreshRecordingProgressTask();
        recordingTimer.schedule(task, 0, 1000);
    }

    private void stopRecording() {
        if (!_isRecording)
            return;

        mMediaRecorder.stop();
        mMediaRecorder.reset();
        stopScreenSharing();
        recordingTimer.cancel();
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.cancel(mId);
        _isRecording = false;
    }

    private VirtualDisplay createVirtualDisplay() {
        return mMediaProjection.createVirtualDisplay("ScreenSharingDemo",
                mDisplayWidth, mDisplayHeight, mScreenDensity,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
                mSurface, null /*Callbacks*/, null /*Handler*/);
    }

    private void createNotification() {
        Date now = Calendar.getInstance().getTime();
        long recordSeconds = (now.getTime() - startRecordingTime.getTime())/1000;
        String recordTimeText=String.format("%02d:%02d",recordSeconds/60,recordSeconds%60);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_media_stop)
                        .setContentTitle("Recording...")
                        .setContentText(recordTimeText);

        Intent resultIntent = new Intent(this, Ac2.class);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );


        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(mId, mBuilder.build());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            showSettings();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showSettings() {
        Intent i = new Intent(this, Prefs.class);
        startActivity(i);
    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        // читаем установленное значение из CheckBoxPreference
        PrefManager.getInstance().Micrphone = prefs.getBoolean("microphone", true);
        PrefManager.getInstance().Shake = prefs.getBoolean("Shake", true);
        PrefManager.getInstance().DropFolder = prefs.getString("dropfolder", "TRecs");
        PrefManager.getInstance().density = prefs.getString("density", "360p");
        PrefManager.getInstance().FormatName = prefs.getString("format", "mp4");
    }
}
